package rabbitmq.queueListerner

import akka.actor.Actor

trait BaseMsg

trait Publisher {
  actor: Actor =>
  def publish(msg: BaseMsg): Unit = {
    actor.context.system.eventStream.publish(msg)
  }
}

trait Subscriber extends Actor {
  def messagesToSubscribe(): Set[Class[_ <: BaseMsg]]

  override def preStart(): Unit = {
    messagesToSubscribe().foreach(context.system.eventStream.subscribe(self, _))
  }
}
