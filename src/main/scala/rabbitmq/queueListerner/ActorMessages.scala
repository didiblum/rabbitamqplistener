package rabbitmq.queueListerner

import com.rabbitmq.client.AMQP.BasicProperties
import com.rabbitmq.client.{Connection, Envelope}

trait ActorMessages
trait BaseMsg

case object StartListening
case object CleanupCache
case class MsgCompleted(tag: Long, resultStatus: Boolean) extends BaseMsg

case class DeadQueueDetails(exchange: String,
                            routingKey: String,
                            expiration: Long)

case class ConsumeDetails(consumeConnection: Connection,
                          queueName: String,
                          prefetch: Int)

case class RabbitMsg(envelope: Envelope,
                     properties: BasicProperties,
                     body: Array[Byte])

trait MsgToWorker extends BaseMsg {
  def deliveryTag: Long
}