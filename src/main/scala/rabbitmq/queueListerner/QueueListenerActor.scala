package rabbitmq.queueListerner

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, Cancellable, Props}
import com.google.common.cache._
import com.rabbitmq.client.AMQP.BasicProperties
import com.rabbitmq.client._
import org.apache.logging.log4j.scala.Logging
import rabbitmq.queueListerner.QueueListenerActor.HandleDelivery
import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._


object QueueListenerActor {

  def props[T <: MsgToWorker](consumeDetails: ConsumeDetails,
                              deadQueueDetails: DeadQueueDetails,
                              handleNewMessage: HandleDelivery[T]): Props = {
    Props(new QueueListenerActor(consumeDetails,
      deadQueueDetails,
      handleNewMessage))
  }

  def encryptedProps[T <: MsgToWorker](consumeDetails: ConsumeDetails,
                                       deadQueueDetails: DeadQueueDetails,
                                       handleNewMessage: HandleDelivery[T]): Props = {
    Props(new QueueListenerActor(consumeDetails,
      deadQueueDetails,
      handleNewMessage))
  }


  type HandleDelivery[T <: MsgToWorker] = (String, Envelope, BasicProperties, Array[Byte]) => T
}

class QueueListenerActor[T <: MsgToWorker](consumeDetails: ConsumeDetails,
                                           deadQueueDetails: DeadQueueDetails,
                                           handleNewMessage: HandleDelivery[T])
  extends Actor
    with Logging
    with Publisher
    with Subscriber {

  private lazy val channel = consumeDetails.consumeConnection.createChannel()
  implicit val ec: ExecutionContextExecutor = context.system.dispatcher

  private val messagesInProgress: Cache[Long, RabbitMsg] = CacheBuilder.newBuilder
    .expireAfterWrite(deadQueueDetails.expiration, TimeUnit.SECONDS)
    .removalListener((removal: RemovalNotification[Long, RabbitMsg]) => {
      if (removal.getCause == RemovalCause.EXPIRED) {
        logger.error(s"Message was not acked. Removing and pushing to dead queue with deliveryTag : ${removal.getKey}")
        channel.basicAck(removal.getKey, false)
        channel.basicPublish(deadQueueDetails.exchange, deadQueueDetails.routingKey, removal.getValue.properties, removal.getValue.body)
      }
    }).build().asInstanceOf[Cache[Long, RabbitMsg]]

  val cleanup: Cancellable = context.system.scheduler.schedule(
    initialDelay = 0.milliseconds,
    interval = deadQueueDetails.expiration.milliseconds,
    self,
    CleanupCache
  )

  override def receive: Receive = {
    case StartListening => startReceiving()
    case CleanupCache => messagesInProgress.cleanUp()
    case MsgCompleted(tag, _) => finishedProcessMsg(tag)
  }

  private def startReceiving(): Unit = {
    logger.info(s"Starting to listen to queue ${consumeDetails.queueName}")

    val consumer = new DefaultConsumer(channel) {
      override def handleDelivery(consumerTag: String, envelope: Envelope,
                                  properties: BasicProperties,
                                  body: Array[Byte]): Unit = {
        newMsgInProgress(envelope.getDeliveryTag, RabbitMsg(envelope, properties, body))
        val msg: T = handleNewMessage(consumerTag, envelope, properties, body)
        publish(msg)
      }
    }

    channel.basicQos(consumeDetails.prefetch)
    channel.basicConsume(
      consumeDetails.queueName,
      false,
      consumer
    )
  }

  override def postStop(): Unit = {
    super.postStop()
    consumeDetails.consumeConnection.close()
    cleanup.cancel()
  }

  def newMsgInProgress(deliveryTag: Long, rabbitMsg: RabbitMsg): Unit = {
    logger.debug(s"Adding message to in progress : $deliveryTag")
    messagesInProgress.put(deliveryTag, rabbitMsg)
  }

  def finishedProcessMsg(deliveryTag: Long): Unit = {
    logger.debug(s"Finished process deliveryTag : $deliveryTag")
    Option(messagesInProgress.getIfPresent(deliveryTag)).foreach { _ =>
      messagesInProgress.invalidate(deliveryTag)
      channel.basicAck(deliveryTag, false)
    }
  }

  override def messagesToSubscribe(): Set[Class[_ <: BaseMsg]] = Set(classOf[MsgCompleted])

}