name := "rabbitAmqpListener"

version := "0.1"

scalaVersion := "2.12.6"

val log4JVersion = "2.11.0"
val akkaVersion = "2.5.12"

libraryDependencies ++= Seq(
  "org.apache.logging.log4j" % "log4j-api" % log4JVersion,
  "org.apache.logging.log4j" % "log4j-jcl" % log4JVersion,
  "org.apache.logging.log4j" % "log4j-slf4j-impl" % log4JVersion,
  "org.apache.logging.log4j" % "log4j-core" % log4JVersion,
  "org.apache.logging.log4j" %% "log4j-api-scala" % "11.0",
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
  "com.google.guava" % "guava" % "25.1-jre",
  "com.rabbitmq" % "amqp-client" % "5.2.0"
)