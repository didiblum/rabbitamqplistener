A RabbitMq queue listener, with the ability to send messages to a dead queue when the processing time reached timeout.

**Usage:**

In order to send the dead messages to the dead queue, you need to provide DeadQueueDetails.
DeadQueueDetails should include the exchange, routing key and the timeout.

In order to send the message to a worker actorm you need to implement HandleDelivery function, to handle the message.
Once the message is done, MsgCompleted should be sent to the listener. 

If MsgCompleted is not send by the time of the provided expiration, the message will be removed from the messages in process queue, and will bep pushed to the dead queue. 
